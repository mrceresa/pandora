######################### Common libraries
#Bring the headers, such as Student.h into the project
include_directories(include include/analysis)

##### Python #
find_package( PythonInterp 2.7 REQUIRED )
find_package(PythonLibs 2.7 REQUIRED)
include_directories(${PYTHON_INCLUDE_DIRS})
link_directories(${PYTHON_LIBRARIES})
message(STATUS "Found python headers " ${PYTHON_INCLUDE_DIRS})
message(STATUS "Found python libs " ${PYTHON_LIBRARIES})
set(version ${PYTHONLIBS_VERSION_STRING})
STRING( REGEX MATCH "[0-9]"  boost_py_version ${version} )

execute_process(COMMAND python-config --libs OUTPUT_VARIABLE FOO)
execute_process(
  COMMAND "${PYTHON_EXECUTABLE}" -c "if True:
    from distutils import sysconfig as sc
    print(sc.get_python_lib(prefix='', plat_specific=True))"
  OUTPUT_VARIABLE PYTHON_SITE
  OUTPUT_STRIP_TRAILING_WHITESPACE)
message(STATUS "python config is: " ${FOO})
message(STATUS "python site is: " ${PYTHON_SITE})
   
############

# Boost stuff
SET(Boost_USE_MULTITHREADED ON)
IF("${boost_py_version}" MATCHES "3")
  SET(BOOST_PYTHON python${boost_py_version})
ELSE()
  SET(BOOST_PYTHON python)
ENDIF()
FIND_PACKAGE(Boost COMPONENTS timer system filesystem chrono program_options date_time ${BOOST_PYTHON} REQUIRED)
INCLUDE_DIRECTORIES(${Boost_INCLUDE_DIRS})

##### MP ####
FIND_PACKAGE(OpenMP REQUIRED)
IF(OPENMP_FOUND)
  SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
  SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
ENDIF()

##### MPI ###
find_package(MPI REQUIRED)
include_directories(${MPI_INCLUDE_PATH})
set(CMAKE_CXX_COMPILE_FLAGS ${CMAKE_CXX_COMPILE_FLAGS} ${MPI_COMPILE_FLAGS})
set(CMAKE_CXX_LINK_FLAGS ${CMAKE_CXX_LINK_FLAGS} ${MPI_LINK_FLAGS})
message(STATUS "MPI compile flags " ${MPI_COMPILE_FLAGS})
message(STATUS "MPI link flags " ${MPI_LINK_FLAGS})

##### GDAL ###
find_package(GDAL REQUIRED)
include_directories(${GDAL_INCLUDE_DIR})
set(GDAL_CONFIG /usr/bin/gdal-config)
exec_program(${GDAL_CONFIG} ARGS --version OUTPUT_VARIABLE GDAL_VERSION)
if(GDAL_CONFIG_LIBS)
  string(REPLACE "." "" GDAL_VERSION_NUM ${GDAL_VERSION})
  message(STATUS "GDAL VERSION IS " ${GDAL_VERSION_NUM})
  if(${GDAL_VERSION_NUM} LESS 210)
    message(STATUS "Adding GDAL_OLD definition to preprocessor")
    add_definitions(-DGDAL_OLD)
  endif()
endif()

#### TinyXML #
if(USE_CUSTOM_TINYXML)
  set(TinyXML_FOUND TRUE)
else()
  find_package(TinyXML REQUIRED)
endif()

if(TinyXML_FOUND)
  include_directories(${TinyXML_INCLUDE_DIRS})
  link_directories(${TinyXML_LIBRARY_DIRS})
  message(STATUS "TinyXML includes:" ${TinyXML_INCLUDE_DIRS})
  message(STATUS "TinyXML library dirs:" ${TinyXML_LIBRARY_DIRS})
  message(STATUS "TinyXML libraries:" ${TinyXML_LIBRARIES})
endif()

##### HDF5 ###
if(USE_CUSTOM_HDF5)
  set(HDF5_FOUND TRUE)
else()
  set(HDF5_PREFER_PARALLEL TRUE)
  find_package(HDF5 COMPONENTS C REQUIRED)
endif()

if(HDF5_FOUND)
  include_directories(${HDF5_INCLUDE_DIRS})
  message(STATUS "HDF5 includes:" ${HDF5_INCLUDE_DIRS})
  message(STATUS "HDF5 libraries:" ${HDF5_LIBRARIES})
ENDIF()

#include_directories("/usr/include/openmpi-x86_64/")

##### OPENSSL ###
find_package(OpenSSL)
if(OPENSSL_FOUND)
  include_directories(${OPENSSL_INCLUDE_DIRS})
  message(STATUS "OPENSSL includes:" ${OPENSSL_INCLUDE_DIRS})
  message(STATUS "OPENSSL libraries:" ${OPENSSL_LIBRARIES})
endIF(OPENSSL_FOUND)


# end Common libraries ####################
