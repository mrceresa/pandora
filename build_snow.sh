#!/bin/bash
set -euo pipefail
IFS=$'\n\t'
echo "**********************************"
echo "* Starting Pandora configuration *"
echo "**********************************"

set -x
CMAKE_CONFIG="Release"
#BUILD_DIR="build-"$CMAKE_CONFIG
BUILD_DIR="build-Release_mc"
BUILD_CASSANDRA="OFF"
USE_CUSTOM_HDF5="OFF"
CMAKE_VERBOSE_MAKEFILE="ON"

set +x

echo "INFO: ============================================================"
echo "INFO: Summary:"
echo "INFO: ============================================================"
echo "INFO: CMAKE_CONFIG               = $CMAKE_CONFIG"
echo "INFO: BUILD_CASSANDRA            = $BUILD_CASSANDRA"
echo "INFO: USE_CUSTOM_HDF5            = $USE_CUSTOM_HDF5"
echo "INFO: CMAKE_VERBOSE_MAKEFILE     = $CMAKE_VERBOSE_MAKEFILE"
echo "INFO: ============================================================"

if [ ! -d $BUILD_DIR ]; then
  mkdir -p $BUILD_DIR
fi

SOURCE_DIR=$(pwd)
pushd $BUILD_DIR

# Loading modules

declare -a arr=("CMake/3.9.5-foss-2017a"
                "HDF5/1.8.17-foss-2017a"
                "Boost/1.61.0-foss-2017a-Python-2.7.12"
                "GDAL/2.1.3-foss-2017a-Python-2.7.12"
                "pkg-config/0.29.1-foss-2017a"
		"cURL/7.49.1-foss-2017a")
#		"OpenSSL/1.0.1s-foss-2017a")

for i in "${arr[@]}"
do
   echo ml load "$i"
   ml load "$i"
done

# Configure
GDAL_INCLUDE_DIR=${EBROOTGDAL}/include 
GDAL_CONFIG=${EBROOTGDAL}/bin/gdal-config
PKG_CONFIG_PATH=/homedtic/mceresa/builds/tinyxml/tinyxml/inst/lib64/pkgconfig/
#TinyXML_INCLUDE_DIRS=/homedtic/mceresa/builds/tinyxml/tinyxml/inst/include
#TinyXML_LIBRARIES="tinyml"
#TinyXML_LIBRARY_DIRS=/homedtic/mceresa/builds/tinyxml/tinyxml/inst/lib64
#PYTHON_INCLUDE="/soft/easybuild/debian/8.8/Broadwell/software/Python/2.7.12-foss-2017a/include/python2.7"
#OPENSSL_INCLUDE_DIR="/soft/easybuild/debian/8.8/Broadwell/software/OpenSSL/1.0.1k-foss-2017a/include/openssl"
#OPENSSL_ROOT_DIR="/soft/easybuild/debian/8.8/Broadwell/software/OpenSSL/1.0.1k-foss-2017a"
#OPENSSL_LIBRARIES="/soft/easybuild/debian/8.8/Broadwell/software/OpenSSL/1.0.1k-foss-2017a/lib"

CC=gcc CXX=g++ cmake -L -DCMAKE_BUILD_TYPE="$CMAKE_CONFIG" \
         -DCMAKE_CXX_FLAGS="-std=c++11 -fpermissive -D_GLIBCXX_USE_CXX11_ABI=0" \
         -DGDAL_INCLUDE_DIR="$GDAL_INCLUDE_DIR" \
         -DGDAL_CONFIG="$GDAL_CONFIG" \
         -DBUILD_CASSANDRA="$BUILD_CASSANDRA" \
         $SOURCE_DIR/
         
make VERBOSE=1 -j8       

popd  
