This is a fork internally used at UPF from the great project by Xavier Rubio that you can find here:

Webpage: https://github.com/xrubio/pandora

* To configure in SNOW cluster, use:

cmake -L -DBUILD_CASSANDRA=OFF -DCMAKE_BUILD_TYPE=Release -DUSE_CUSTOM_HDF5=ON -DHDF5_INCLUDE_DIRS=$HOME/hdf5_openmpi/include -DHDF5_LIBRARIES=$HOME/hdf5_openmpi/lib/libhdf5.so ../



