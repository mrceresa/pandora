
include_directories(${CMAKE_CURRENT_SOURCE_DIR})

set(generateMPI ${PANDORA_MPIGEN})

set(mpiAgentsSrc mpiCode/FactoryCode.cxx mpiCode/Human_mpi.cxx)
set(mpiAgentsSrcFullPath ${CMAKE_CURRENT_SOURCE_DIR}/mpiCode/FactoryCode.cxx ${CMAKE_CURRENT_SOURCE_DIR}/mpiCode/Human_mpi.cxx)
set(AgentsSrc 'main.cxx' 'Human.cxx')
set(AgentsEnv '{"namespaces":["Examples"]}')

MESSAGE( STATUS "AgentsEnv:         " ${AgentsEnv} )
MESSAGE( STATUS "AgentsSrc:         " ${mpiAgentsSrc} )

file(MAKE_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/mpiCode)
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/mpiCode)

add_custom_command(
  OUTPUT ${mpiAgentsSrcFullPath}
  COMMAND ${PYTHON_EXECUTABLE} ${generateMPI} --target ${mpiAgentsSrc} --source ${AgentsSrc} --env ${AgentsEnv}
  WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
)

file(GLOB SOURCES "*.cxx")

add_executable(epidemy ${SOURCES} ${mpiAgentsSrcFullPath})
target_link_libraries(epidemy pandora)

file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/config.xml
    DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
        )
        
file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/resources
     DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
        )
        
install(TARGETS epidemy DESTINATION ~/usr/bin)
